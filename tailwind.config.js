// tailwind.config.js
const plugin = require('tailwindcss/plugin')

module.exports = {
  variants: {
    backgroundColor: ({after}) => after(['invalid']),
    margin: ['active'],
  },
  plugins: [
    plugin(function({ addVariant, e }) {
      addVariant('invalid', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`invalid${separator}${className}`)}:not(:placeholder-shown):invalid`
        })
      })
    })
  ]
}
