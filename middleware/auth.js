const publicURLs = [
  'sign-up',
  'sign-in',
]

export default ({ route, store, redirect }) => {
  if (publicURLs.includes(route.name)) {
    return
  }

  const expired = store.state.profile.error === 'jwt expired'
  if (expired) {
    store.dispatch('clear')
  }

  if (!store.getters['profile/isLoggedIn'] || expired) {
    redirect('/sign-in')
  }
}
