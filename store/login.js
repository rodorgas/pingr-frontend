export const state = () => ({
  status: {
    error: '',
    status: 'not_logged_in'
  },
})

export const mutations = {
  setAccessToken(state, accessToken) {
    state.accessToken = accessToken
  },

  setStatus(state, { status, error }) {
    state.status = { status, error }
  },

  resetStatus(state) {
    state.status = { error: '', status: 'not_logged_in'}
  },
}

export const actions = {
  async login(context, { email, password }) {
    let response

    try {
      response = await this.$axios.post('signin', {
        email, password
      })
    }
    catch (error) {
      context.commit(
        'setStatus',
        { error: error.response.data, status: 'error' }
      )

      return
    }

    context.commit('setStatus', { error: '', status: 'ok' })
    context.commit('profile/setAccessToken', response.data.accessToken, {root: true})
  },

  resetStatus(context) {
    context.commit('resetStatus')
  },
}
