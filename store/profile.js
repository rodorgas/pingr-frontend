export const state = () => ({
  accessToken: '',
  username: '',
  email: '',
  error: '',
})

export const mutations = {
  setAccessToken(state, accessToken) {
    state.accessToken = accessToken
  },

  setUserData(state, { email, username }) {
    state.username = username
    state.email = email
  },

  setError(state, error) {
    state.error = error
  }
}

export const actions = {
  setAccessToken(context, accessToken) {
    context.commit('setAccessToken', accessToken)
  },

  async fetchUserData(context, id) {
    if (context.state.accessToken === '') {
      return
    }

    let resp
    try {
      resp = await this.$axios.$get(`600/users/${id}`, {
        headers: {
          Authorization: `Bearer ${context.state.accessToken}`
        }
      })
    }
    catch (error) {
      context.commit('setError', error.response.data)

      return
    }

    context.commit('setError', '')

    context.commit('setUserData', {
      email: resp.email,
      username: resp.username,
    })
  },

  clear(context) {
    context.commit('setAccessToken', '')
    context.commit('setUserData', { email: '', username: ''})
    context.commit('setError', '')
  }
}

export const getters = {
  isLoggedIn(state) {
    return state.accessToken !== ''
  },

  id(state) {
    if (state.accessToken === '') {
      return undefined
    }

    const encoded = state.accessToken.split('.')[1]
    const buffer = Buffer.from(encoded, 'base64')
    const data = JSON.parse(buffer.toString())

    return data.sub
  }
}
